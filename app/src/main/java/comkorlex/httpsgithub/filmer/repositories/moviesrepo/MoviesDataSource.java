package comkorlex.httpsgithub.filmer.repositories.moviesrepo;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;
import android.util.Log;


import java.util.List;

import javax.inject.Inject;

import comkorlex.httpsgithub.filmer.data.db.DbHelper;
import comkorlex.httpsgithub.filmer.data.entities.movies.MovieItem;
import comkorlex.httpsgithub.filmer.data.entities.movies.MoviesResponse;
import comkorlex.httpsgithub.filmer.data.prefs.PrefsHelper;
import comkorlex.httpsgithub.filmer.data.remote.TmdbService;
import comkorlex.httpsgithub.filmer.repositories.NetworkState;
import comkorlex.httpsgithub.filmer.repositories.Status;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

import static comkorlex.httpsgithub.filmer.data.remote.TmdbService.API_KEY_V3;

public class MoviesDataSource extends PageKeyedDataSource<Integer, MovieItem> {

    private TmdbService tmdbService;
    private DbHelper dbHelper;
    private PrefsHelper prefsHelper;

    private String state;

    private boolean refresh;

    private MutableLiveData<NetworkState> networkState;
    private MutableLiveData<NetworkState> initialLoad;


    private Completable retryCompletable;


    public MoviesDataSource(TmdbService tmdbService, DbHelper dbHelper, PrefsHelper prefsHelper, String state, boolean refresh) {
        this.tmdbService = tmdbService;
        this.dbHelper = dbHelper;
        this.prefsHelper = prefsHelper;
        this.state = state;
        this.refresh = refresh;
        initDatas();
    }


    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, MovieItem> callback) {
        Log.d("INIT LOAD", "INIT LOAD");

        initialLoad.postValue(NetworkState.LOADING);

        tmdbService.loadMovies(state, API_KEY_V3, "en-US",1)
                .subscribe(moviesResponse -> {

                    initialLoad.postValue(NetworkState.NET_LOADED);
                    List<MovieItem> movies = moviesResponse.getResults();
                    prefsHelper.putMoviesMenuState(state);
                    dbHelper.refreshMovies(movies);
                    callback.onResult(movies, null, moviesResponse.getPage()+1);

                }, throwable-> {

                    Log.d("INIT LOAD", "throwable");

                    if(!refresh && !checkMenuDiffers(state)) {
                        Log.d("INIT LOAD", "LOAD from db");
                        initialLoad.postValue(NetworkState.DB_LOADED);
                        callback.onResult(dbHelper.getMoviesList(),null,null);
                    } else {
                        Log.d("INIT LOAD", "show error");
                        NetworkState error = NetworkState.error(throwable.getMessage());
                        // publish the error
                        initialLoad.postValue(error);
                    }

                });


    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, MovieItem> callback) {
        // not yet
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, MovieItem> callback) {
        networkState.postValue(NetworkState.LOADING);

        tmdbService.loadMovies(state, API_KEY_V3, "en-US",1)
                .subscribe(moviesResponse -> {

                    setRetry(null);
                    networkState.postValue(NetworkState.NET_LOADED);
                    callback.onResult(moviesResponse.getResults(), moviesResponse.getPage()+1 );

                }, throwable -> {

                    setRetry(()->loadAfter(params, callback));
                    networkState.postValue(NetworkState.error(throwable.getMessage()));

                });


    }


    public void retry(){
        if(retryCompletable!=null){
            retryCompletable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe();
        }
    }


    public MutableLiveData<NetworkState> getNetworkState() {
        return networkState;
    }


    public MutableLiveData<NetworkState> getInitialLoad() {
        return initialLoad;
    }


    private void initDatas(){
        networkState = new MutableLiveData<>();
        initialLoad = new MutableLiveData<>();
    }


    private void setRetry(final Action action) {
        retryCompletable = (action == null) ? null : Completable.fromAction(action);
    }

    private boolean checkMenuDiffers(String menuState){
        if(!menuState.equals(prefsHelper.getMoviesMenuState())) return true;
        else return false;
    }


}
