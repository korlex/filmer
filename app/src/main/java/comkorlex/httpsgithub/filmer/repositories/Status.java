package comkorlex.httpsgithub.filmer.repositories;

public enum Status {
    RUNNING,
    SUCCESS,
    FAILED,
    NET_SUCCESS,
    DB_SUCCESS
}
