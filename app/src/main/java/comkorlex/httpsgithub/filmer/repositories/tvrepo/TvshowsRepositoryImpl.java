package comkorlex.httpsgithub.filmer.repositories.tvrepo;

import javax.inject.Inject;

import comkorlex.httpsgithub.filmer.data.remote.TmdbService;
import comkorlex.httpsgithub.filmer.repositories.tvrepo.TvshowsRepository;

public class TvshowsRepositoryImpl implements TvshowsRepository {

    private TmdbService tmdbService;


    @Inject
    public TvshowsRepositoryImpl(TmdbService tmdbService) {
        this.tmdbService = tmdbService;
    }
}
