package comkorlex.httpsgithub.filmer.repositories.peoplerepo;

import javax.inject.Inject;

import comkorlex.httpsgithub.filmer.data.remote.TmdbService;
import comkorlex.httpsgithub.filmer.repositories.peoplerepo.PeopleRepository;

public class PeopleRepositoryImpl implements PeopleRepository {

    private TmdbService tmdbService;

    @Inject
    public PeopleRepositoryImpl(TmdbService tmdbService) {
        this.tmdbService = tmdbService;
    }
}
