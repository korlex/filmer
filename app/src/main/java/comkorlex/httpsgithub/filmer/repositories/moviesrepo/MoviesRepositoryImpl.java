package comkorlex.httpsgithub.filmer.repositories.moviesrepo;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.paging.DataSource;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.util.Log;

import javax.inject.Inject;
import javax.inject.Provider;

import comkorlex.httpsgithub.filmer.data.db.DbHelper;
import comkorlex.httpsgithub.filmer.data.entities.movies.MovieItem;
import comkorlex.httpsgithub.filmer.data.entities.movies.MoviesResponse;
import comkorlex.httpsgithub.filmer.data.prefs.PrefsHelper;
import comkorlex.httpsgithub.filmer.data.remote.TmdbService;
import comkorlex.httpsgithub.filmer.repositories.LoadResult;
import comkorlex.httpsgithub.filmer.repositories.NetworkState;
import comkorlex.httpsgithub.filmer.repositories.moviesrepo.MoviesRepository;
import io.reactivex.Flowable;

import static comkorlex.httpsgithub.filmer.data.remote.TmdbService.API_KEY_V3;

public class MoviesRepositoryImpl implements MoviesRepository {

    private TmdbService tmdbService;
    private PagedList.Config config;
    private DbHelper dbHelper;
    private PrefsHelper prefsHelper;


    private MoviesDataSourceFactory factory;


    @Inject
    public MoviesRepositoryImpl(TmdbService tmdbService, DbHelper dbHelper,PrefsHelper prefsHelper, PagedList.Config config) {
        this.tmdbService = tmdbService;
        this.dbHelper = dbHelper;
        this.prefsHelper = prefsHelper;
        this.config = config;
    }


    @Override
    public String fetchMenuState() {
        return prefsHelper.getMoviesMenuState();
    }

    @Override
    public LoadResult<MovieItem> fetchMovies(String state){

        factory = new MoviesDataSourceFactory(tmdbService, dbHelper, prefsHelper, state);

        LiveData<NetworkState> networkState = Transformations.switchMap(factory.getMoviesDataSourceLive(), moviesDs-> moviesDs.getNetworkState());
        LiveData<NetworkState> initialLoad = Transformations.switchMap(factory.getMoviesDataSourceLive(), moviesDs-> moviesDs.getInitialLoad());

        LiveData<PagedList<MovieItem>> data = new LivePagedListBuilder<Integer, MovieItem>(factory, config).build();


        return new LoadResult<MovieItem>(data, networkState, initialLoad);
    }




    // хз пока здесь ли ... -_-
    @Override
    public void refresh(){
        Log.d("REPO IM HERE", "IAM HERE");
        factory.invalidateDs();
    }

    @Override
    public void retry(){
        if(factory!=null) factory.getMoviesDataSourceLive().getValue().retry();
    }


}
