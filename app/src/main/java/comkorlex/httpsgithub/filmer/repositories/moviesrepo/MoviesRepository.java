package comkorlex.httpsgithub.filmer.repositories.moviesrepo;


import comkorlex.httpsgithub.filmer.data.entities.movies.MovieItem;
import comkorlex.httpsgithub.filmer.repositories.LoadResult;

public interface MoviesRepository {

    public String fetchMenuState();
    LoadResult<MovieItem> fetchMovies(String state);
    void refresh();
    void retry();

}
