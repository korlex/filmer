package comkorlex.httpsgithub.filmer.repositories;

public class NetworkState {

    private Status status;

    private String message;

    private NetworkState(Status status, String message) {
        this.status = status;
        this.message = message;
    }

    private NetworkState(Status status) {
        this.status = status;
    }


    // to del ?
    public static NetworkState LOADED = new NetworkState(Status.SUCCESS);

    public static NetworkState NET_LOADED = new NetworkState(Status.NET_SUCCESS);

    public static NetworkState DB_LOADED = new NetworkState(Status.DB_SUCCESS);

    public static NetworkState LOADING = new NetworkState(Status.RUNNING);

    public static NetworkState error(String message) {
        return new NetworkState(Status.FAILED, message == null ? "unknown error" : message);
    }


    public Status getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }



    public boolean loadFinished(){
        if (status == Status.NET_SUCCESS || status == Status.DB_SUCCESS || status == Status.FAILED) return true;
        return false;
    }

    public boolean isLoaded(){
        if(status == Status.NET_SUCCESS || status == Status.DB_SUCCESS ) return true;
        return false;
    }

    public boolean isNetLoaded(){
        if (status == Status.NET_SUCCESS) return true;
        return false;
    }


    public boolean isDbLoaded(){
        if(status == Status.DB_SUCCESS) return true;
        return false;
    }

    public boolean isFailed(){
        if (status == Status.FAILED) return true;
        return false;
    }


}
