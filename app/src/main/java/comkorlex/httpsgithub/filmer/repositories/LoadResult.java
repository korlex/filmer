package comkorlex.httpsgithub.filmer.repositories;

import android.arch.lifecycle.LiveData;
import android.arch.paging.PagedList;

public class LoadResult<T> {

    private LiveData<PagedList<T>> data;
    private LiveData<NetworkState> networkState;
    private LiveData<NetworkState> initialLoad;


    public LoadResult(LiveData<PagedList<T>> data, LiveData<NetworkState> networkState, LiveData<NetworkState> initialLoad) {
        this.data = data;
        this.networkState = networkState;
        this.initialLoad = initialLoad;
    }


    public LiveData<PagedList<T>> getData() {
        return data;
    }

    public LiveData<NetworkState> getNetworkState() {
        return networkState;
    }

    public LiveData<NetworkState> getInitialLoad() {
        return initialLoad;
    }

}
