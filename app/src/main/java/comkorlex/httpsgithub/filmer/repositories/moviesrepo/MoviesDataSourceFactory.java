package comkorlex.httpsgithub.filmer.repositories.moviesrepo;


import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import comkorlex.httpsgithub.filmer.data.db.DbHelper;
import comkorlex.httpsgithub.filmer.data.entities.movies.MovieItem;
import comkorlex.httpsgithub.filmer.data.prefs.PrefsHelper;
import comkorlex.httpsgithub.filmer.data.remote.TmdbService;
import comkorlex.httpsgithub.filmer.repositories.moviesrepo.MoviesDataSource;

public class MoviesDataSourceFactory extends DataSource.Factory<Integer, MovieItem> {


    private TmdbService tmdbService;
    private DbHelper dbHelper;
    private PrefsHelper prefsHelper;
    private String state;
    private boolean refresh = false;
    private MutableLiveData<MoviesDataSource> moviesDataSourceLive;


    public MoviesDataSourceFactory(TmdbService tmdbService, DbHelper dbHelper, PrefsHelper prefsHelper, String state) {
        this.tmdbService = tmdbService;
        this.dbHelper = dbHelper;
        this.prefsHelper = prefsHelper;
        this.state = state;
        moviesDataSourceLive = new MutableLiveData<>();
    }


    @Override
    public DataSource create() {
        MoviesDataSource moviesDataSource = new MoviesDataSource(tmdbService, dbHelper, prefsHelper, state, refresh);
        moviesDataSourceLive.postValue(moviesDataSource);
        return moviesDataSource;
    }


    public void invalidateDs(){
        refresh = true;
        moviesDataSourceLive.getValue().invalidate();
    }

    public MutableLiveData<MoviesDataSource> getMoviesDataSourceLive() {
        return moviesDataSourceLive;
    }

}
