package comkorlex.httpsgithub.filmer.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import comkorlex.httpsgithub.filmer.R;
import comkorlex.httpsgithub.filmer.repositories.NetworkState;
import comkorlex.httpsgithub.filmer.repositories.Status;

public class NetStateViewHolder extends RecyclerView.ViewHolder {

    private RetryCallback retryCallback;
    private TextView errorMsg;
    private ProgressBar loadingBar;
    private Button retryBtn;

    // make with binding ... later

    public NetStateViewHolder(View v, RetryCallback retryCallback) {
        super(v);
        this.retryCallback = retryCallback;
        errorMsg = (TextView) v.findViewById(R.id.item_netstate_errorMessage);
        loadingBar = (ProgressBar) v.findViewById(R.id.item_netstate_loading);
        retryBtn = (Button) v.findViewById(R.id.item_netstate_retry);
    }

    void bindNetworkState(NetworkState networkState){
        //error message
        errorMsg.setVisibility(networkState.getMessage() != null ? View.VISIBLE : View.GONE);
        if (networkState.getMessage() != null) {
            errorMsg.setText(networkState.getMessage());
        }

        //loading and retry
        loadingBar.setVisibility(networkState.getStatus() == Status.RUNNING ? View.VISIBLE : View.GONE);
        retryBtn.setVisibility(networkState.getStatus() == Status.FAILED ? View.VISIBLE : View.GONE);
        if(retryCallback!=null) retryBtn.setOnClickListener(view->retryCallback.retry());
    }


    public static NetStateViewHolder create(ViewGroup parent, RetryCallback retryCallback){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_net_state, parent, false);
        return new NetStateViewHolder(v, retryCallback);
    }


}
