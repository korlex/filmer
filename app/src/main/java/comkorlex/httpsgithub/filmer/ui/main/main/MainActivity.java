package comkorlex.httpsgithub.filmer.ui.main.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

import com.android.databinding.library.baseAdapters.BR;

import javax.inject.Inject;

import comkorlex.httpsgithub.filmer.R;
import comkorlex.httpsgithub.filmer.databinding.ActivityMainBinding;
import comkorlex.httpsgithub.filmer.ui.base.BaseActivity;
import comkorlex.httpsgithub.filmer.ui.navigation.MainNavigationController;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class MainActivity extends BaseActivity implements HasSupportFragmentInjector, DrawerLayout.DrawerListener, ViewPager.OnPageChangeListener{

    @Inject DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;
    @Inject MainNavigationController navigationController;
    @Inject SectionsAdapter sectionsAdapter;
    @Inject Handler handler;
    private ActivityMainBinding binding;
    private MainViewModel mainViewModel;
    private Runnable navClosePendingRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        binding.setVariable(BR.view, this);
        binding.setViewmodel(mainViewModel);

        setToolbar();
        initNavDrawer();
        initViewPagerWithTabs();

        test();


        //binding.mainDrawerLayout.addDrawerListener();

    }

    private void setToolbar(){
        setSupportActionBar(binding.mainToolbar);
    }

    private void initNavDrawer(){
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, binding.mainDrawerLayout, binding.mainToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        binding.mainDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        //binding.mainNavView.setNavigationItemSelectedListener(null);

    }

    private void initViewPagerWithTabs(){
        binding.mainViewpager.addOnPageChangeListener(this);
        binding.mainViewpager.setAdapter(sectionsAdapter);
        binding.mainTabs.setupWithViewPager(binding.mainViewpager);
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    private void test(){
        mainViewModel.getNavViewEvent().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String s) {
                // create runnable
                navClosePendingRunnable = new Runnable() {
                    @Override
                    public void run() {
                        navigationController.navigateTo(s);
                    }
                };

                binding.mainDrawerLayout.closeDrawer(binding.mainNavView);

            }
        });

    }

    @Override
    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {}

    @Override
    public void onDrawerOpened(@NonNull View drawerView) {}

    @Override
    public void onDrawerClosed(@NonNull View drawerView) {
        // run runnable
        if(navClosePendingRunnable != null){
            handler.post(navClosePendingRunnable);
            navClosePendingRunnable = null;
        }
    }

    @Override
    public void onDrawerStateChanged(int newState) {}



    // rewrite menu on InvalidateOptionsMenu ?

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        SubMenu subMenu = menu.findItem(R.id.activity_main_toolbar_menu).getSubMenu();
        subMenu.clear();

        switch (binding.mainViewpager.getCurrentItem()){
            case 0:
                getMenuInflater().inflate(R.menu.menu_movies, subMenu);
                break;
            case 1:
                getMenuInflater().inflate(R.menu.menu_tvshows, subMenu);
                break;
            case 2:
                getMenuInflater().inflate(R.menu.menu_people, subMenu);
                break;
        }
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d("TAG", "ON CREATE OPTIONS MENU");
        getMenuInflater().inflate(R.menu.activity_main_toolbar, menu);
        return true;
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    // DA

    @Override
    public void onPageSelected(int position) {
        invalidateOptionsMenu();
    }


    @Override
    public void onPageScrollStateChanged(int state) {}



    //        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.addDrawerListener(toggle);
//        toggle.syncState();
//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);


}
