package comkorlex.httpsgithub.filmer.ui.main.main;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

public class MainViewModel extends ViewModel {

    private MutableLiveData<String> navViewEvent;

    public MainViewModel() {
        navViewEvent = new MutableLiveData<>();
    }

    public boolean test(String menuItemName){
        navViewEvent.setValue(menuItemName);
        return true;

    }

    public MutableLiveData<String> getNavViewEvent() {
        return navViewEvent;
    }
}
