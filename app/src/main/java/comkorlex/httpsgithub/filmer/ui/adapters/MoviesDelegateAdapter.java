package comkorlex.httpsgithub.filmer.ui.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import comkorlex.httpsgithub.filmer.R;
import comkorlex.httpsgithub.filmer.data.entities.movies.MovieItem;
import comkorlex.httpsgithub.filmer.databinding.ItemMovieBinding;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;


public class MoviesDelegateAdapter implements DelegateAdapter {


    private ItemMovieBinding itemMovieBinding;


    @Inject
    public MoviesDelegateAdapter() {
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemMovieBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_movie, parent, false);
        return new MoviesViewHolder(itemMovieBinding);
    }


    @Override
    public void onBindViewHolder(Object obj, RecyclerView.ViewHolder holder) {
        MovieItem movieItem = (MovieItem) obj;
        MoviesViewHolder moviesViewHolder = (MoviesViewHolder) holder;
        moviesViewHolder.bindMovie(movieItem);
    }

    @Override
    public boolean isForViewType(Object obj) {
        return obj instanceof MovieItem;
    }



    private class MoviesViewHolder extends RecyclerView.ViewHolder{

        private ItemMovieBinding itemMovieBinding;

        public MoviesViewHolder(ItemMovieBinding itemMovieBinding) {
            super(itemMovieBinding.getRoot());
            this.itemMovieBinding = itemMovieBinding;

        }

        void bindMovie(MovieItem movieItem){
            itemMovieBinding.setMovie(movieItem);
        }

    }

}
