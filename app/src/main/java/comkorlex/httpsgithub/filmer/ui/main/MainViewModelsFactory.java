package comkorlex.httpsgithub.filmer.ui.main;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Provider;
import comkorlex.httpsgithub.filmer.di.scopes.MainFragmentsScope;


@MainFragmentsScope
public class MainViewModelsFactory implements ViewModelProvider.Factory {

    private Map<Class<? extends ViewModel>, ViewModel> viewModels;

    @Inject
    public MainViewModelsFactory(Map<Class<? extends ViewModel>, ViewModel> viewModels) {
        this.viewModels = viewModels;
    }



    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
//        Provider<ViewModel> viewModelProvider = viewModels.get(modelClass);
//
//        if (viewModelProvider == null) {
//            throw new IllegalArgumentException("model class "
//                    + modelClass
//                    + " not found");
//        }
//
//        return (T) viewModelProvider.get();


        ViewModel viewModel = viewModels.get(modelClass);
        if(viewModel == null){
            throw new IllegalArgumentException("model class "
                    + modelClass
                    + " not found");
        } else return (T) viewModel;

    }


}
