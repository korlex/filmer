package comkorlex.httpsgithub.filmer.ui.main.tv;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import comkorlex.httpsgithub.filmer.R;
import comkorlex.httpsgithub.filmer.data.entities.TestTvshow;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class TvshowsFragment extends Fragment {

//    @Inject
//    @Named("Tvshows")
//    ListDelegationAdapter listDelegationAdapter;

    private RecyclerView testTvshowsList;
    private List<TestTvshow> testTvshows;

    public static TvshowsFragment newInstance(){
        return new TvshowsFragment();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        AndroidSupportInjection.inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_tvshows, container, false);

//        initTestTvShows();
//
//        initList(v);

        return v;
    }


//    private void initList(View v){
//        testTvshowsList = (RecyclerView) v.findViewById(R.id.test_tvshows_list);
//        testTvshowsList.setLayoutManager(new LinearLayoutManager(getActivity()));
//        testTvshowsList.setAdapter(listDelegationAdapter);
//        listDelegationAdapter.swapItems(testTvshows);
//    }


    private void initTestTvShows(){
        testTvshows = new ArrayList<>();
        testTvshows.add(new TestTvshow("Breaking bad", "HBO","5","50 minutes" ));
        testTvshows.add(new TestTvshow("Walking dead", "HBO", "8", "45 minutes"));
    }

}
