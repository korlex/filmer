package comkorlex.httpsgithub.filmer.ui.main.movies;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.PagedList;
import android.util.Log;

import javax.inject.Inject;

import comkorlex.httpsgithub.filmer.data.entities.movies.MovieItem;
import comkorlex.httpsgithub.filmer.repositories.LoadResult;
import comkorlex.httpsgithub.filmer.repositories.NetworkState;
import comkorlex.httpsgithub.filmer.repositories.moviesrepo.MoviesRepository;
import comkorlex.httpsgithub.filmer.utils.Function2;
import comkorlex.httpsgithub.filmer.utils.LiveDataUtils2;

public class MoviesViewModel extends ViewModel{

    private MoviesRepository moviesRepository;
    private MutableLiveData<String> menuItemChoice;

    private LiveData<LoadResult<MovieItem>> loadResultLive;

    private LiveData<PagedList<MovieItem>> moviesLive;
    private LiveData<NetworkState> networkState;
    private LiveData<NetworkState> initialLoad;

    private LiveData<Boolean> emptyState;


    @Inject
    public MoviesViewModel(MoviesRepository moviesRepository) {
        this.moviesRepository = moviesRepository;
        initDatas();

    }


    public void fetchInitial(){menuItemChoice.setValue(moviesRepository.fetchMenuState());}

    public void setMenuItemChoice(String input){ menuItemChoice.setValue(input); }

    public void retry(){moviesRepository.retry();}

    public void refresh(){
        Log.d("VM IM HERE", "IAM HERE");
        moviesRepository.refresh();
    }

    public LiveData<PagedList<MovieItem>> getMoviesLive() {
        return moviesLive;
    }

    public LiveData<NetworkState> getNetworkState() {
        return networkState;
    }

    public LiveData<NetworkState> getInitialLoad(){ return initialLoad; }

    public LiveData<Boolean> getEmptyState() { return emptyState; }

    private void initDatas(){

        menuItemChoice = new MutableLiveData<>();

        loadResultLive = Transformations.map(menuItemChoice,(input -> moviesRepository.fetchMovies(input)));


        moviesLive = Transformations.switchMap(loadResultLive, (loadRes-> loadRes.getData()));
        networkState = Transformations.switchMap(loadResultLive, (loadRes-> loadRes.getNetworkState()));
        initialLoad = Transformations.switchMap(loadResultLive, loadRes->{
            Log.d("initLOad", "inirLOad");
            return loadRes.getInitialLoad();});


//        emptyState = LiveUtils4Kt.zipLiveData(moviesLive, initialLoad, new Function2<PagedList<MovieItem>, NetworkState, Boolean>() {
//            @Override
//            public Boolean apply(PagedList<MovieItem> input1, NetworkState input2) {
//                if(input1.snapshot().size()== 0 && input2.getStatus()== Status.SUCCESS) return true;
//                return false;
//            }
//        });

        emptyState = LiveDataUtils2.zip(moviesLive, initialLoad, new Function2<PagedList<MovieItem>, NetworkState, Boolean>() {
            @Override
            public Boolean apply(PagedList<MovieItem> input1, NetworkState input2) {
                Log.d("LIVEDATAUTILS","APPLY");
                if(input1.isEmpty() && input2.loadFinished()) return true;
                return false;
            }
        });

    }


}
