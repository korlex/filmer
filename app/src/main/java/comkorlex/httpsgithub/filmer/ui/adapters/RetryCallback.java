package comkorlex.httpsgithub.filmer.ui.adapters;

public interface RetryCallback {
    void retry();
}
