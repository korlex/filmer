package comkorlex.httpsgithub.filmer.ui.adapters;

import android.arch.paging.PagedListAdapter;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.ViewGroup;

import comkorlex.httpsgithub.filmer.R;
import comkorlex.httpsgithub.filmer.repositories.NetworkState;
import comkorlex.httpsgithub.filmer.ui.adapters.DelegateAdapter;
import comkorlex.httpsgithub.filmer.ui.adapters.MoviesDelegateAdapter;

public class PagedListDelegationAdapter extends PagedListAdapter {

    private SparseArray<DelegateAdapter> adapters;
    private int adaptersCount;
    private int typeView;
    private NetworkState networkState;
    private RetryCallback retryCallback;


    public PagedListDelegationAdapter(@NonNull DiffUtil.ItemCallback diffCallback, MoviesDelegateAdapter moviesDelegateAdapter) {
        super(diffCallback);
        adapters = new SparseArray<>();
        adapters.put(adaptersCount++, moviesDelegateAdapter);
    }


    @Override
    public int getItemViewType(int position) {

        if(isNetworkStateTime(position)) return R.layout.item_net_state;

        for (int i = 0; i <adapters.size() ; i++) {
            DelegateAdapter delegateAdapter = adapters.valueAt(i);

            if(delegateAdapter.isForViewType(getItem(position))){
                typeView = adapters.keyAt(i);
                return typeView;
            }
        }

        throw new NullPointerException(
                "Can not get viewType for position " + position);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == R.layout.item_net_state) return NetStateViewHolder.create(parent, retryCallback);
        return adapters.get(viewType).onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof NetStateViewHolder) ((NetStateViewHolder) holder).bindNetworkState(networkState);
        else adapters.get(typeView).onBindViewHolder(getItem(position), holder);

    }


    @Override
    public int getItemCount() {
        return super.getItemCount() + (hasExtraRow() ? 1 : 0);
    }





    public void setRetryCallback(RetryCallback retryCallback) {
        this.retryCallback = retryCallback;
    }

    public void setNetworkState(NetworkState newNetworkState) {
        Log.d("SET NETWORK STATE", newNetworkState.getStatus().name());
        if (getCurrentList() != null) {
            if (getCurrentList().size() != 0) {
                NetworkState previousState = this.networkState;
                boolean hadExtraRow = hasExtraRow();
                this.networkState = newNetworkState;
                boolean hasExtraRow = hasExtraRow();
                if (hadExtraRow != hasExtraRow) {
                    if (hadExtraRow) {
                        notifyItemRemoved(super.getItemCount());
                    } else {
                        notifyItemInserted(super.getItemCount());
                    }
                } else if (hasExtraRow && previousState != newNetworkState) {
                    notifyItemChanged(getItemCount() - 1);
                }
            }
        }
    }




    private Boolean hasExtraRow() {
        return networkState != null && networkState != NetworkState.LOADED;
    }

    private boolean isNetworkStateTime(int pos){
        if (hasExtraRow() && pos == getItemCount() - 1) return true;
        return false;
    }



}
