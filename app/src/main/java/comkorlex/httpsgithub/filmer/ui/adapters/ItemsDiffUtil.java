package comkorlex.httpsgithub.filmer.ui.adapters;

import android.support.v7.util.DiffUtil;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ItemsDiffUtil extends DiffUtil.ItemCallback{

    @Inject
    public ItemsDiffUtil() {
    }

    @Override
    public boolean areItemsTheSame(Object oldItem, Object newItem) {
        if(oldItem == newItem) return true;
        return false;
    }

    @Override
    public boolean areContentsTheSame(Object oldItem, Object newItem) {
        if(oldItem == newItem) return true;
        return false;
    }

}