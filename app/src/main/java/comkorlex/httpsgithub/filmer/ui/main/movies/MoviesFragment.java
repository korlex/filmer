package comkorlex.httpsgithub.filmer.ui.main.movies;


import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import javax.inject.Inject;
import javax.inject.Named;

import comkorlex.httpsgithub.filmer.R;
import comkorlex.httpsgithub.filmer.databinding.FragmentMoviesBinding;
import comkorlex.httpsgithub.filmer.repositories.Status;
import comkorlex.httpsgithub.filmer.ui.adapters.PagedListDelegationAdapter;
import comkorlex.httpsgithub.filmer.ui.base.BaseFragment;
import comkorlex.httpsgithub.filmer.ui.main.MainViewModelsFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoviesFragment extends BaseFragment {

    @Inject
    @Named("Movies")
    PagedListDelegationAdapter adapter;
    @Inject
    MainViewModelsFactory mainViewModelsFactory;


    private FragmentMoviesBinding binding;
    private MoviesViewModel moviesViewModel;
    private RecyclerView moviesList;
    private String menuItem;

    public static MoviesFragment newInstance(){
        return new MoviesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        Log.d("MOVIES FRAG", "ON CREATE VIEW");

        setHasOptionsMenu(true);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movies, container, false);
        moviesViewModel = ViewModelProviders.of(this, mainViewModelsFactory).get(MoviesViewModel.class);
        binding.setViewModel(moviesViewModel);
        binding.setLifecycleOwner(this);
        //binding.setShowContent(true);

        initList();
        initSubscriptions();



        return binding.getRoot();
    }


    @Override
    public void onStart() {
        super.onStart();
        moviesViewModel.fetchInitial();
    }



    private void initList(){
        moviesList = binding.moviesList;
        moviesList.setLayoutManager(new LinearLayoutManager(getActivity()));
        moviesList.setAdapter(adapter);
        adapter.setRetryCallback(()->moviesViewModel.retry());
    }

    private void initSubscriptions(){
        moviesViewModel.getMoviesLive().observe(this, (movieItems -> {
            // прилетают данные в любом случае .. даже без callback on result
            Log.d("SIZE", "size = " + movieItems.snapshot().size());
            if(!movieItems.isEmpty())adapter.submitList(movieItems);
            //binding.setShowContent(checkAdaperHasList());
        }));
        moviesViewModel.getNetworkState().observe(this, (networkState -> adapter.setNetworkState(networkState)));
        moviesViewModel.getInitialLoad().observe(this, networkState -> {

            if(networkState.loadFinished()) binding.moviesSwipeRefresh.setRefreshing(false);
            if(networkState.isLoaded()) binding.testStatus.setText(menuItem);
            if(networkState.isFailed()) Toast.makeText(getActivity(), networkState.getMessage(), Toast.LENGTH_SHORT).show();
        });

        //moviesViewModel.getEmptyState().observe(this, b-> Toast.makeText(getActivity(),b.toString(), Toast.LENGTH_SHORT).show());

    }


    private boolean checkAdaperHasList(){
        return adapter.getCurrentList() != null ? true:false;
    }


    // for test

//    @Override
//    public void onItemClick(Object obj) {
//        Log.d("TAG", "ITEM CLICK");
//        MovieItem movieItem = (MovieItem) obj;
//        Toast.makeText(getActivity(), movieItem.getTitle(), Toast.LENGTH_SHORT ).show();
//    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        menuItem = item.getTitle().toString();

        switch (item.getItemId()){
            case R.id.menu_movies_popular:
                moviesViewModel.setMenuItemChoice("popular");
                break;
            case R.id.menu_movies_toprated:
                moviesViewModel.setMenuItemChoice("top_rated");
                break;
            case R.id.menu_movies_upcoming:
                moviesViewModel.setMenuItemChoice("upcoming");
                break;
            case R.id.menu_movies_nowplaying:
                moviesViewModel.setMenuItemChoice("now_playing");
                break;
        }

        return true;

    }
}
