package comkorlex.httpsgithub.filmer.ui.base;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.Bindable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.util.Log;
import android.view.View;

public abstract class BaseViewModel extends ViewModel {

    // хз зачем всё это было надо но пока оставим 31.18


    private final ObservableBoolean dataLoading = new ObservableBoolean(false);
    private final ObservableBoolean error = new ObservableBoolean(false);
    private final ObservableBoolean content = new ObservableBoolean(false);
    private final ObservableField<String> infoMsg = new ObservableField<>();


    public ObservableBoolean getDataLoading() {
        return dataLoading;
    }

    public ObservableBoolean getError() {
        return error;
    }

    public ObservableBoolean getContent() {
        return content;
    }

    public ObservableField<String> getInfoMsg() {
        return infoMsg;
    }


    // СКРЫВАТЬ НЕ НУЖНОЕ

    public void setDataLoading(){
        dataLoading.set(true);
    }

    public void setError(){
        content.set(false);
        dataLoading.set(false);
        error.set(true);
    }

    public void setContent(){
        error.set(false);
        dataLoading.set(false);
        content.set(true);
    }

    public void setInfoMsg(String msg){
        infoMsg.set(msg);
    }

    public abstract void reloadData(View v);

}
