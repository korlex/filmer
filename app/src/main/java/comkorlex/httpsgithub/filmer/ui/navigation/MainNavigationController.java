package comkorlex.httpsgithub.filmer.ui.navigation;

import android.content.Intent;
import android.support.v4.util.ArrayMap;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import comkorlex.httpsgithub.filmer.di.scopes.MainScope;
import comkorlex.httpsgithub.filmer.ui.about.AboutActivity;
import comkorlex.httpsgithub.filmer.ui.main.main.MainActivity;
import comkorlex.httpsgithub.filmer.ui.settings.SettingsActivity;

@MainScope
public class MainNavigationController {

    private Map<String, Class<?>> activities;
    private MainActivity mainActivity;


    @Inject
    public MainNavigationController(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        activities = new ArrayMap<>();
        activities.put("Settings", SettingsActivity.class);
        activities.put("About", AboutActivity.class);
    }


    public void navigateTo(String activityName){
        Class<?> requiredActivity = activities.get(activityName);
        Intent intent = new Intent(mainActivity, requiredActivity);
        mainActivity.startActivity(intent);
    }

}
