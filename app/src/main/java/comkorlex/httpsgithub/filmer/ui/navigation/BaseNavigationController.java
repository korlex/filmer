//package comkorlex.httpsgithub.filmer.ui.navigation;
//
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentTransaction;
//
//import comkorlex.httpsgithub.filmer.R;
//
//public abstract class BaseNavigationController {
//
//    protected void addFragment(FragmentManager fragmentManager, int containerId, Fragment frag, boolean isFirst){
//        FragmentTransaction ft = fragmentManager.beginTransaction();
//        if(!isFirst){
//            ft.hide(fragmentManager.findFragmentById(R.id.main_frag_container));
//            ft.addToBackStack(null);
//        }
//        ft.add(containerId, frag).commit();
//    }
//
//}
