package comkorlex.httpsgithub.filmer.ui.main.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import javax.inject.Inject;

import comkorlex.httpsgithub.filmer.R;
import comkorlex.httpsgithub.filmer.di.scopes.MainScope;
import comkorlex.httpsgithub.filmer.ui.main.artists.ArtistsFragment;
import comkorlex.httpsgithub.filmer.ui.main.movies.MoviesFragment;
import comkorlex.httpsgithub.filmer.ui.main.tv.TvshowsFragment;

@MainScope
public class SectionsAdapter extends FragmentStatePagerAdapter {

    private MainActivity mainActivity;

    @Inject
    public SectionsAdapter(MainActivity mainActivity) {
        super(mainActivity.getSupportFragmentManager());
        this.mainActivity = mainActivity;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return mainActivity.getResources().getString(R.string.tab_movies);
            case 1:
                return mainActivity.getResources().getString(R.string.tab_tvshows);
            case 2:
                return mainActivity.getResources().getString(R.string.tab_artists);
            default:
                return null;
        }

    }



    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return MoviesFragment.newInstance();
            case 1:
                return TvshowsFragment.newInstance();
            case 2:
                return ArtistsFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
