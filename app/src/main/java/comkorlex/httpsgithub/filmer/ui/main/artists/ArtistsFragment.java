package comkorlex.httpsgithub.filmer.ui.main.artists;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import comkorlex.httpsgithub.filmer.R;
import comkorlex.httpsgithub.filmer.data.entities.TestBaseModel;
import comkorlex.httpsgithub.filmer.data.entities.TestMovie;
import comkorlex.httpsgithub.filmer.data.entities.TestTvshow;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArtistsFragment extends Fragment {

//    @Inject
//    @Named("Shows")
//    ListDelegationAdapter listDelegationAdapter;


    private RecyclerView testArtistsList;
    private List<TestBaseModel> testBaseModels;


    public static ArtistsFragment newInstance(){
        return new ArtistsFragment();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        AndroidSupportInjection.inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_artists, container, false);

//        initTestBaseModels();
//
//        initList(v);

        return v;
    }

//    private void initList(View v){
//        testArtistsList = (RecyclerView) v.findViewById(R.id.test_artists_list);
//        testArtistsList.setLayoutManager(new LinearLayoutManager(getActivity()));
//        testArtistsList.setAdapter(listDelegationAdapter);
//        listDelegationAdapter.swapItems(testBaseModels);
//    }

    private void initTestBaseModels(){
        testBaseModels = new ArrayList<>();
        testBaseModels.add(new TestMovie("Sharlok Holmes", "2 hours"));
        testBaseModels.add(new TestTvshow("Breaking bad", "HBO","5","50 minutes" ));
        testBaseModels.add(new TestMovie("Intouchables ", "1.40 hours"));
        testBaseModels.add(new TestTvshow("Walking dead", "HBO", "8", "45 minutes"));
        testBaseModels.add(new TestMovie("Léon", "1.5 hours"));
    }


}
