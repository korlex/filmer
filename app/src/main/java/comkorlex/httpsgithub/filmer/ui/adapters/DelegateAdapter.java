package comkorlex.httpsgithub.filmer.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

public interface DelegateAdapter {

    // viewtype то не нужен здесь по сути
    RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType);
    void onBindViewHolder(Object obj, RecyclerView.ViewHolder holder);
    boolean isForViewType(Object obj);
}
