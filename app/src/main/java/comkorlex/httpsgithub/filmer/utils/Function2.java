package comkorlex.httpsgithub.filmer.utils;

public interface Function2<I1,I2,O> {
    O apply(I1 input1, I2 input2);
}
