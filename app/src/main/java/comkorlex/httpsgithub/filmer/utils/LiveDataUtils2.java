package comkorlex.httpsgithub.filmer.utils;

import android.arch.lifecycle.LiveData;
import android.arch.core.util.Function;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.util.Log;


public class LiveDataUtils2 {

    private static final String UTILS2 = "UTILS2";
    private static Object lastA;
    private static Object lastB;

    private LiveDataUtils2(){}

    public static <A, B, C> LiveData<C> zip(LiveData<A> src1, LiveData<B> src2, Function2<A,B,C> func2){

        Log.d("zipping","zipping");

        lastA = null;
        lastB = null;

        final MediatorLiveData<C> result = new MediatorLiveData<>();
        result.addSource(src1,a->{
           lastA = a;
           loging();
            if (lastA!=null && lastB!=null) {
                result.setValue(func2.apply((A)lastA,(B)lastB));
                lastA = null;}
        });
        result.addSource(src2,b->{
            lastB = b;
            loging();
            if (lastA!=null && lastB!=null) {
                result.setValue(func2.apply((A)lastA,(B)lastB));
                lastB = null;}
        });

        return result;
    }


    private static void reset(){
        lastA = null;
        lastB = null;
    }

    private static void loging(){
        if(lastA==null) Log.d(UTILS2, "lastA is null");
        else Log.d(UTILS2, "lastA is NOT null");
        if(lastB == null) Log.d(UTILS2, "lastB is null");
        else Log.d(UTILS2, "lastB is NOT null");
    }


}
