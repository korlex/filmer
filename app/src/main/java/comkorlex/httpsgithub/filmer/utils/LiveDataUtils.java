package comkorlex.httpsgithub.filmer.utils;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;

public class LiveDataUtils<A,B> {

    private A lastA = null;
    private B lastB = null;
    //private Pair<A,B> pair;


    public LiveData<Pair<A,B>> zipLIveData(LiveData<A> live1, LiveData<B> live2){


        MediatorLiveData <Pair<A,B>> m = new MediatorLiveData<>();
        m.addSource(live1, new Observer<A>() {
            @Override
            public void onChanged(@Nullable A a) {
                lastA = a;
                update(m.getValue());
            }
        });

        m.addSource(live2, new Observer<B>() {
            @Override
            public void onChanged(@Nullable B b) {
                lastB = b;
                update(m.getValue());
            }
        });

        return m;
    }


    private void update(Pair<A,B> value){
        A localA = lastA;
        B localB = lastB;
        if (localA != null && localB != null) value = new Pair<>(localA, localB);
    }




    // new Mediator , add src1, add src2

}
