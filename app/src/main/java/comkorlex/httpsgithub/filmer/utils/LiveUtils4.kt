package comkorlex.httpsgithub.filmer.utils

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData

fun <A, B, C> zipLiveData(a: LiveData<A>, b: LiveData<B>, func2: Function2<A,B,C>): LiveData<C> {
    return MediatorLiveData<C>().apply {
        var lastA: A? = null
        var lastB: B? = null

        fun update() {
            val localLastA = lastA
            val localLastB = lastB
            if (localLastA != null && localLastB != null)
                this.value = func2.apply(localLastA, localLastB);
        }

        addSource(a) {
            lastA = it
            update()
        }
        addSource(b) {
            lastB = it
            update()
        }
    }


}