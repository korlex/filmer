package comkorlex.httpsgithub.filmer.data.db;

import java.util.List;

import comkorlex.httpsgithub.filmer.data.entities.movies.MovieItem;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.internal.operators.single.SingleJust;
import io.reactivex.schedulers.Schedulers;

public class DbHelper {

//    helper class for easy db access


    private TmdbDao tmdbDao;

    public DbHelper(TmdbDao tmdbDao) {
        this.tmdbDao = tmdbDao;
    }

//    public Disposable refreshMovies(List<MovieItem> movieItems){
//        return Completable
//                .fromAction(()->tmdbDao.refreshMovies(movieItems))
//                .subscribeOn(Schedulers.io())
//                .subscribe();
//    }


    public void refreshMovies(List<MovieItem> movieItems){
        tmdbDao.refreshMovies(movieItems);
    }

    public List<MovieItem> getMoviesList(){
        return tmdbDao.getMoviesList();
    }



}
