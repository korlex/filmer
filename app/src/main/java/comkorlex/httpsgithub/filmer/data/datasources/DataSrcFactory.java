package comkorlex.httpsgithub.filmer.data.datasources;

import android.arch.paging.DataSource;

public class DataSrcFactory extends DataSource.Factory{

    private int requiredSrc;

    public DataSrcFactory(int requiredSrc) {
        this.requiredSrc = requiredSrc;
    }

    @Override
    public DataSource create() {
        switch (requiredSrc){
            case 1:
                return new MoviesDataSrc();
            default:
                return null;
        }
    }
}
