package comkorlex.httpsgithub.filmer.data.datasources;

import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;

import comkorlex.httpsgithub.filmer.data.remote.TmdbService;

public class MoviesDataSrc extends PageKeyedDataSource {

    private TmdbService tmdbService;

    @Override
    public void loadInitial(@NonNull LoadInitialParams params, @NonNull LoadInitialCallback callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams params, @NonNull LoadCallback callback) {

    }

    @Override
    public void loadBefore(@NonNull LoadParams params, @NonNull LoadCallback callback) {

    }


}
