package comkorlex.httpsgithub.filmer.data.remote;

import comkorlex.httpsgithub.filmer.data.entities.movies.MoviesResponse;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TmdbService {

    static final String API_READ_ACCESS_TOKEN_V4 = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIyZWE5N2UzYTZhZjUwMzIyZTJjNmQ3MDU2MzVlODljZCIsInN1YiI6IjVhNzRjNzg5MGUwYTI2NjQzMDAyOTdkZCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.RHwaTNwVb11eCMIpW0i36Xeif3ziNJS2hKnaqbTg9Wc";
    static final String API_KEY_V3 = "2ea97e3a6af50322e2c6d705635e89cd";
    static final String API_URL = "https://api.themoviedb.org/3/";


    // movies top rated, popular , upcoming etc..
    // https://api.themoviedb.org/3/movie/popular?api_key=2ea97e3a6af50322e2c6d705635e89cd&language=en-US&page=1
    // api docs
    // https://developers.themoviedb.org/3/authentication/how-do-i-generate-a-session-id



    // movies

    @GET(("movie/{path}"))
    Single<MoviesResponse> loadMovies(@Path("path") String path,
                                      @Query("api_key") String apiKey,
                                      @Query("language") String language,
                                      @Query("page") int page);








}
