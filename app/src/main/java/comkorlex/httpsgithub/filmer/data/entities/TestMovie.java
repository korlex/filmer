package comkorlex.httpsgithub.filmer.data.entities;

public class TestMovie implements TestBaseModel {

    private String mavieName;
    private String movieLength;


    public TestMovie(String mavieName, String movieLength) {
        this.mavieName = mavieName;
        this.movieLength = movieLength;
    }

    public String getMavieName() {
        return mavieName;
    }

    public void setMavieName(String mavieName) {
        this.mavieName = mavieName;
    }

    public String getMovieLength() {
        return movieLength;
    }

    public void setMovieLength(String movieLength) {
        this.movieLength = movieLength;
    }
}
