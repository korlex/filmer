package comkorlex.httpsgithub.filmer.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefsHelper {

    private static final String PREFS_NAME = "filmer prefs";
    private static final String MOVIES_MENU_STATE="movies menu state";
    private static final String TV_MENU_STATE = "tv menu state";
    private static final String PEOPLE_MENU_STATE = "people menu state";

    private SharedPreferences prefs;


    public PrefsHelper(Context context) {
        prefs = context.getSharedPreferences("FILMER PREFS", Context.MODE_PRIVATE);
    }


    public void putMoviesMenuState(String moviesMenuState){
        prefs.edit().putString(MOVIES_MENU_STATE, moviesMenuState).apply();
    }

    public String getMoviesMenuState(){
        return prefs.getString(MOVIES_MENU_STATE, "popular");
    }




}
