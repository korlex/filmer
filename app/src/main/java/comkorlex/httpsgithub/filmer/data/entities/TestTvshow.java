package comkorlex.httpsgithub.filmer.data.entities;

public class TestTvshow implements TestBaseModel {

    private String tvShowName;
    private String tvChannel;
    private String seasonsCount;
    private String episodeLength;

    public TestTvshow(String tvShowName, String tvChannel, String seasonsCount, String episodeLength) {
        this.tvShowName = tvShowName;
        this.tvChannel = tvChannel;
        this.seasonsCount = seasonsCount;
        this.episodeLength = episodeLength;
    }

    public String getTvShowName() {
        return tvShowName;
    }

    public void setTvShowName(String tvShowName) {
        this.tvShowName = tvShowName;
    }

    public String getTvChannel() {
        return tvChannel;
    }

    public void setTvChannel(String tvChannel) {
        this.tvChannel = tvChannel;
    }

    public String getSeasonsCount() {
        return seasonsCount;
    }

    public void setSeasonsCount(String seasonsCount) {
        this.seasonsCount = seasonsCount;
    }

    public String getEpisodeLength() {
        return episodeLength;
    }

    public void setEpisodeLength(String episodeLength) {
        this.episodeLength = episodeLength;
    }
}
