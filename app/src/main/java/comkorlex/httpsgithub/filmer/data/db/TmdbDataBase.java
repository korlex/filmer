package comkorlex.httpsgithub.filmer.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import comkorlex.httpsgithub.filmer.data.entities.movies.MovieItem;

@Database(entities = {MovieItem.class}, version = 1)
public abstract class TmdbDataBase extends RoomDatabase {

    public abstract TmdbDao getTmdbDao();

}
