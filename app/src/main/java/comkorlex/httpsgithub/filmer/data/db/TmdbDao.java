package comkorlex.httpsgithub.filmer.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import java.util.List;

import comkorlex.httpsgithub.filmer.data.entities.movies.MovieItem;
import io.reactivex.Maybe;

@Dao
public abstract class TmdbDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract void putMovies(List<MovieItem> movies);

    @Query("DELETE FROM movies")
    abstract void clearMovies();


    @Transaction
    void refreshMovies(List<MovieItem> movies){
        clearMovies();
        putMovies(movies);
    }

    @Query("SELECT * FROM movies")
    abstract List<MovieItem> getMoviesList();




}
