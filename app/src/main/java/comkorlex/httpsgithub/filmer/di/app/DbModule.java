package comkorlex.httpsgithub.filmer.di.app;

import android.app.Application;
import android.arch.persistence.room.Room;

import javax.inject.Singleton;

import comkorlex.httpsgithub.filmer.data.db.DbHelper;
import comkorlex.httpsgithub.filmer.data.db.TmdbDao;
import comkorlex.httpsgithub.filmer.data.db.TmdbDataBase;
import dagger.Module;
import dagger.Provides;

@Module
public class DbModule {

    @Provides
    @Singleton
    public TmdbDataBase provideTmdbDataBase(Application applicationContext){
        return Room.databaseBuilder(applicationContext, TmdbDataBase.class, "Tmdb.db").build();
    }

    @Provides
    @Singleton
    public TmdbDao provideTmdbDao(TmdbDataBase tmdbDataBase){
        return tmdbDataBase.getTmdbDao();
    }

    @Provides
    @Singleton
    public DbHelper provideDbHelper(TmdbDao tmdbDao){
        return new DbHelper(tmdbDao);
    }

}
