package comkorlex.httpsgithub.filmer.di.app;

import android.app.Application;

import javax.inject.Singleton;

import comkorlex.httpsgithub.filmer.data.prefs.PrefsHelper;
import dagger.Module;
import dagger.Provides;

@Module
public class PrefsModule {

    @Singleton
    @Provides
    public PrefsHelper providePrefsHelper(Application applicationContext){
        return new PrefsHelper(applicationContext);
    }


}
