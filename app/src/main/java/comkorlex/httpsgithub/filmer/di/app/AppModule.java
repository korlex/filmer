package comkorlex.httpsgithub.filmer.di.app;


import android.app.Application;
import android.arch.paging.PagedList;
import android.os.Handler;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {


//    @Provides
//    @Singleton
//    public Application provideAppContext(Application application){
//        return application;
//    }

    @Provides
    @Singleton
    public Handler provideHandler(){
        return new Handler();
    }


    @Provides
    @Singleton
    public PagedList.Config providePagedListConfig(){
        return new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setInitialLoadSizeHint(20)
                .setPageSize(20)
                .setPrefetchDistance(5)
                .build();
    }


}
