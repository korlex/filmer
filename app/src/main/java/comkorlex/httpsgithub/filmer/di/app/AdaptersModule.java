package comkorlex.httpsgithub.filmer.di.app;

import javax.inject.Named;

import comkorlex.httpsgithub.filmer.ui.adapters.ItemsDiffUtil;
import comkorlex.httpsgithub.filmer.ui.adapters.MoviesDelegateAdapter;
import comkorlex.httpsgithub.filmer.ui.adapters.PagedListDelegationAdapter;
import dagger.Module;
import dagger.Provides;

@Module
public class AdaptersModule {

    @Provides
    @Named("Movies")
    public PagedListDelegationAdapter provideListDelegationAdapter(ItemsDiffUtil itemsDiffUtil, MoviesDelegateAdapter moviesDelegateAdapter){
        return new PagedListDelegationAdapter(itemsDiffUtil, moviesDelegateAdapter);
    }


//    @Provides
//    @Named("Tvshows")
//    public ListDelegationAdapter provideListDelegationAdapter1(TvshowsDelegateAdapter tvshowsDelegateAdapter){
//        return new ListDelegationAdapter(tvshowsDelegateAdapter);
//    }
//
//
//    @Provides
//    @Named("Shows")
//    public ListDelegationAdapter provideListDelegationAdapter2(MoviesDelegateAdapter moviesDelegateAdapter, TvshowsDelegateAdapter tvshowsDelegateAdapter){
//        return new ListDelegationAdapter(moviesDelegateAdapter, tvshowsDelegateAdapter);
//    }


}
