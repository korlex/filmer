package comkorlex.httpsgithub.filmer.di.app;

import android.app.Application;

import javax.inject.Singleton;

import comkorlex.httpsgithub.filmer.App;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Component(modules = {
        AppModule.class,
        ActivitiesBuilderModule.class,
        AdaptersModule.class,
        RemoteModule.class,
        DbModule.class,
        PrefsModule.class,
        RepositoriesModule.class,
        AndroidInjectionModule.class})
@Singleton
public interface AppComponent {

    @Component.Builder
    interface Builder{
        @BindsInstance
        Builder application (Application application);
        AppComponent build();
    }

    void inject(App app);
}
