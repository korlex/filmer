package comkorlex.httpsgithub.filmer.di.app;

import comkorlex.httpsgithub.filmer.di.main.MainFragmentsBuilderModule;
import comkorlex.httpsgithub.filmer.di.scopes.MainScope;
import comkorlex.httpsgithub.filmer.ui.main.main.MainActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.multibindings.StringKey;

@Module
public abstract class ActivitiesBuilderModule {

    @ContributesAndroidInjector(modules = MainFragmentsBuilderModule.class)
    @MainScope
    abstract MainActivity contributeMainActivity();


}
