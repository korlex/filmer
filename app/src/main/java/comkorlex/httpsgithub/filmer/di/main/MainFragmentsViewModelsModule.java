package comkorlex.httpsgithub.filmer.di.main;

import android.arch.lifecycle.ViewModel;

import comkorlex.httpsgithub.filmer.di.scopes.MainFragmentsScope;
import comkorlex.httpsgithub.filmer.ui.main.artists.ArtistsViewModel;
import comkorlex.httpsgithub.filmer.ui.main.main.MainViewModel;
import comkorlex.httpsgithub.filmer.ui.main.movies.MoviesViewModel;
import comkorlex.httpsgithub.filmer.ui.main.tv.TvshowsViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class MainFragmentsViewModelsModule {

    @Binds
    @IntoMap
    @MainFragmentsViewModelsKey(MoviesViewModel.class)
    abstract ViewModel bindMoviesViewModel(MoviesViewModel moviesViewModel);


    @Binds
    @IntoMap
    @MainFragmentsViewModelsKey(TvshowsViewModel.class)
    abstract ViewModel bindTvshowsViewMOdel(TvshowsViewModel tvshowsViewModel);

    @Binds
    @IntoMap
    @MainFragmentsViewModelsKey(ArtistsViewModel.class)
    abstract ViewModel bindArtistsViewModel(ArtistsViewModel artistsViewModel);


}
