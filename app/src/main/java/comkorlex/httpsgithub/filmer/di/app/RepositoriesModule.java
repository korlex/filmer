package comkorlex.httpsgithub.filmer.di.app;

import javax.inject.Singleton;

import comkorlex.httpsgithub.filmer.repositories.moviesrepo.MoviesRepository;
import comkorlex.httpsgithub.filmer.repositories.peoplerepo.PeopleRepository;
import comkorlex.httpsgithub.filmer.repositories.tvrepo.TvshowsRepository;
import comkorlex.httpsgithub.filmer.repositories.moviesrepo.MoviesRepositoryImpl;
import comkorlex.httpsgithub.filmer.repositories.peoplerepo.PeopleRepositoryImpl;
import comkorlex.httpsgithub.filmer.repositories.tvrepo.TvshowsRepositoryImpl;
import dagger.Binds;
import dagger.Module;

@Module
public interface RepositoriesModule {

    @Binds
    @Singleton
    MoviesRepository bindsMoviesRepository(MoviesRepositoryImpl moviesRepositoryImpl);

    @Binds
    @Singleton
    PeopleRepository bindsPeopleRepository(PeopleRepositoryImpl peopleRepositoryImpl);


    @Binds
    @Singleton
    TvshowsRepository bindsTvshowsRepository(TvshowsRepositoryImpl tvshowsRepositoryImpl);


}
