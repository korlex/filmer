package comkorlex.httpsgithub.filmer.di.main;

import comkorlex.httpsgithub.filmer.di.scopes.MainFragmentsScope;
import comkorlex.httpsgithub.filmer.ui.main.artists.ArtistsFragment;
import comkorlex.httpsgithub.filmer.ui.main.movies.MoviesFragment;
import comkorlex.httpsgithub.filmer.ui.main.tv.TvshowsFragment;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

// Builds subcomponents for main fragments

@Module
public abstract class MainFragmentsBuilderModule {

    @ContributesAndroidInjector(modules = MainFragmentsViewModelsModule.class)
    @MainFragmentsScope
    abstract MoviesFragment contributeMovieFragment();

    @ContributesAndroidInjector
    @MainFragmentsScope
    abstract TvshowsFragment contributeTvshowsFragment();

    @ContributesAndroidInjector
    @MainFragmentsScope
    abstract ArtistsFragment contributeArtistsFragment();

}
