package comkorlex.httpsgithub.filmer.binding;

import android.databinding.BindingAdapter;
import android.databinding.BindingConversion;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class BindingUtils {

    @BindingAdapter("app:imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        Picasso.with(imageView.getContext())
                .load("https://image.tmdb.org/t/p/w185" + url)
                .into(imageView);
    }


    // все double в string , вызывать не надо


//    @BindingConversion
//    public static String setAvgVote(double voteAverage){
//        if(voteAverage != 0){
//            return String.valueOf(voteAverage/2);
//        } else return "0";
//    }


}
